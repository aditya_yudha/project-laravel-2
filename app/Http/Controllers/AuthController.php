<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('form');
    }
    public function welcome(){
        return view('welcome1');
    }
    public function get_name(Request $request){
        $nama_depan = $request['name'];
        $nama_belakang = $request['last_name'];
        return view('welcome1',['nama_depan'=> $nama_depan,'nama_belakang'=>$nama_belakang]);
    }
}
