<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create()
    {
        return view('cast.tambah');
    }

    public function store(Request $request)
    {
        $validate = $request->validate([
            'nama_cast'=>'required',
            'umur_cast'=>'required',
            'text_cast'=>'required'
        ]);
        
        DB::table('cast')->insert([
            'nama'=>$request['nama_cast'],
            'umur'=>$request['umur_cast'],
            'text'=>$request['text_cast'],
        ]);
        return redirect ('/cast');
    }
    public function index(Request $request)
    {
        $cast = DB::table('cast')->get();
        return view('cast.showData',['cast'=>$cast]);
    }
    public function show($id)
    {
        $cast = DB::table('cast')->where('id',$id)->first();
        return view('cast.detail',['cast'=>$cast]);
    }

    public function edit($id)
    {
        $cast = DB::table('cast')->where('id',$id)->first();
        return view('cast.edit',['cast'=>$cast]);
    }
    public function update(Request $request,$id)
    {
        $validate = $request->validate([
            'nama_cast'=>'required',
            'umur_cast'=>'required',
            'text_cast'=>'required'
        ]);
        DB::table('cast')
            ->where('id',$id)
            ->update(
                ['nama'=>$request->nama_cast,
                'umur'=>$request->umur_cast,
                'text'=>$request->text_cast]
            );
        return redirect('/cast');
    }
    public function destroy($id)
    {
        DB::table('cast')->where('id',$id)->delete();
        return redirect('/cast');
    }
}
