@extends('layout.master')
@section('judul')
Edit Cast
@endsection

@section('content')
<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('PUT')
    <fieldset>
    <legend>Legend</legend>
    <div class="form-group">
        <label class="form-label mt-4">Cast Name</label>
        <input type="text" class="form-control" value="{{$cast->nama}}" name="nama_cast" placeholder="Cast Name">
    </div>
    @error('nama_cast')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group">
        <label class="form-label mt-4">Age</label>
        <input type="number" name ="umur_cast" value="{{$cast->umur}}" class="form-control" placeholder="Age" min="1" max="100" >
    </div>
    @error('umur_cast')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group">
        <label class="form-label mt-4">Text</label>
        <textarea name="text_cast" class="form-control" placeholder="text">{{$cast->text}}</textarea>
    </div>
    @error('text_cast')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
    </fieldset>
</form>
@endsection