<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[HomeController::class,'home']);
Route::get('/form',[AuthController::class,'register']);
Route::post('/welcome',[AuthController::class,'get_name']);

Route::get('/master',function(){
    return view('layout.master');
});
Route::get('/table',function(){
    return view('partial.table');
});
Route::get('/data-table',function(){
    return view('partial.data_table');
});

// CRUD CAST
Route::get('/cast/create',[CastController::class,'create']);
// add new cast to the data base
Route::post('/cast',[CastController::class,'store']);
// show cast data
Route::get('/cast',[CastController::class,'index']);
// Detail Cast base on id
Route::get('/cast/{cast_id}',[CastController::class,'show']);
//update
Route::get('/cast/{cast_id}/edit',[CastController::class,'edit']);
// Update base on id
Route::put('/cast/{cast_id}',[CastController::class,'update']);
// Delete base on id
Route::delete('/cast/{cast_id}',[CastController::class,'destroy']);